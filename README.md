# Airport Currency Exchange Office

Please note that this is the most authoritative document in the repository. I have other notes scattered throughout
the issues and wiki in the repo, but if there are any contridictions, this README should be considered correct.

The app is currently deployed to: https://ntan-currency-exchange.herokuapp.com/

~~Deployments were debugged via local git pushes, but final tests and deployments are all through Pipelines.~~

**Note on deployments:** Lesson learned, plan deployments better next time. More time should have been taken to figure
out how to setup heroku, remotes, and CI to have them all work together nicely. As should be apparent from the pipelines
history, I was having issues with local vs. ci deployments and settling on deploying the final product via local git push.

## Quick Start

```bash
# Clone the repository and install:
git clone https://bitbucket.org/nevantan/currency-exchange.git
cd currency-exchange
yarn install

# Run the tests:
yarn test

# Start the dev server
yarn dev

# Page available at: http://localhost:1234
```

### Development Notes

* Initial configuration can be set in `config.json`, though changes to that file won't be reflected until the
  app is reset
* Resetting the app is just a matter of clearing localStorage
* Redux hasn't been set up to handle the HMR that Parcel uses, so a full refresh may be required while using the
  dev server

## Assumptions

These are a few assumptions I'm making in designing the system, along with changes to be made were they not true.

### The application is running locally, on a secure system, with all-local state, and all users are priviledged

These assumptions are necessary, particularly having a secure system with priviledged users, as there isn't really
a way to validate or control access to data without building a back-end. While it may be possible to use client-
side encryption to implement the access control (if not access levels), I believe this is beyond the scope of
the app and will just leave it here as a suggestion for future enhancement.

This assumption is feasible if we imagine the application is controlled by an employee behind a desk that enters
buy/sell orders into the system on behalf of the customers that approach the counter.

### The list of currencies is known and supported

Based on this assumption, I'm not checking whether the API supports a requested currency. This also means that errors
due to a non-existant currency are not handled. This, especially, should be kept in mind while modifying `config.json`.

## Design and Architecture Notes

### Header Design
I did end up copying the header from Loom with only a few modifications. Notably, the logo has been moved to the left side
of the header because, as [this](https://www.nngroup.com/articles/centered-logos/) article notes, having the logo anywhere
else can make navigating to the homepage up to 6x harder. I have also made adjustments to the spacing at various breakpoints
to make it more mobile friendly.

### Folder Structure
The names of the directories should be fairly self-explanatory, but I wanted to mention a few intentional design choices.

React components are given a dedicated diretory which includes the component source (`index.js`), CSS-in-JS styles (`style.js`),
unit tests (`Component.test.js`) and its StorybookJS story (`story.js`). This structure is recursed down via the (`components/`)
subdirectory which will contain any subcomponents organized in the same manner.

### Data Flow and Business Rule Validation
Data is manipulated in two places: React components for display purposes and Sagas for validation purposes. The latter will
handle display-specific modifications, such as `.toFixed()` rounding, while the latter is for business rules, specifically.

Components will typically dispatch a `VALIDATE_*` or similar action that will be picked up by a saga, transformed, then forwarded
as a `SET_*` action.

## Next Steps
There is a fair bit of code that didn't make it into this build due to time constraints, I'll note them below.

### Full User Input Validation
At present, most of the user input is validated via validator sagas, but I didn't have time to fill in the rest of the validation
for the administration page. The `validateMargin` and `validateCommisionPct` sagas should give a good idea of how that would be
accomplished.

### Error Messages
The current app does validate data bad inputs are properly rejected, but there isn't currenly any messaging to indicate specific
errors to the end user. This would likely involve splitting current application state out into its own store and adding error
messages to display there. Those messages could be displayed using a flash-message library where dismissing the prompt will dispatch
an action to remove the specific error from the store.

Somewhat related to validation errors, network errors would need to be displayed via a mechanism similar to the above. Network errors
simply log to the console right now as there isn't a proper error messaging system in place. For network failures in particular,
exponential back-off should be implemented.

### Filtering and Sorting
The header design of the CurrencyTable component has sort arrows on it, but the functionality isn't actually implemented. In order
to make it easier to users to find what they're looking for, allowing them to sort by any of the columns, or even filter by currency
ID would be a good idea to improve usability. Implementing this could be accomplished through selectors in the `mapStateToProps` method
of the CurrencyTable component.

### Page Transitions
The current transition between the homepage and the admin page is rather jarring. Adding something like a slide animation between
the two would make it easier to process, particulary on mobile. The react-transition-group or animejs would be good candidate solutions.

### Fetch Now Button and Update Settings
Making changes to admin settings such as Refresh Rate and Margin Percentage don't actually take effect until the next time the fetchRates
saga runs. Changes could be made to have these values take effect immediately, or at least implement a "fetch now" button that would
allow admins to manually refetch rates on demand.

### Show Active Page
Writing this up after deploying the final version of my app, I realized that the navigation doesn't actually show which page the user
is currently on. Rectifying this woud involve passing a prop to `<Header />` with the current page retreived from react-router-dom via
the match prop.