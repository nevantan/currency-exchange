import React from 'react';
import { ThemeProvider } from 'react-jss';

export default (theme) => (story) => (
  <ThemeProvider theme={theme}>{story()}</ThemeProvider>
);
