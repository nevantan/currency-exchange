// Storybook config
import { configure, addDecorator } from '@storybook/react';

// Styles and Layout
import withTheme from './withTheme';
import theme from '../src/theme';

// Redux Store
import withRedux from './withRedux';
import createStore from '../src/redux';
const store = createStore();

const loadStories = () => {
  addDecorator(withTheme(theme));
  addDecorator(withRedux(store));

  // Header
  require('../src/components/Header/story.js');
  require('../src/components/Header/components/NavButton/story.js');

  // Admin Page
  require('../src/pages/AdminPage/story.js');
  require('../src/components/AdminSetting/story.js');

  // Home Page Components
  require('../src/pages/HomePage/story.js');
  require('../src/components/BuySellModal/story.js');
  require('../src/components/ModeToggle/story.js');
  require('../src/components/CurrencyTable/story.js');
  require('../src/components/CurrencyTable/components/LastUpdated/story.js');
  require('../src/components/CurrencyTable/components/CurrencyHead/story.js');
  require('../src/components/CurrencyTable/components/CurrencyRow/story.js');
};

configure(loadStories, module);
