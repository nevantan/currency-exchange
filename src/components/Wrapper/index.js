// Libraries
import React from 'react';
import injectSheet from 'react-jss';

// Styles
import styles from './style';

export class Wrapper extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { children, classes = {} } = this.props;

    return <div className={classes.wrapper}>{children}</div>;
  }
}

export default injectSheet(styles)(Wrapper);
