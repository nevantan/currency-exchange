export default (theme) => ({
  wrapper: {
    margin: '0 auto',
    maxWidth: '80rem',
    width: '100%'
  }
});
