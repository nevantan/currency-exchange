import React from 'react';
import { shallow } from 'enzyme';

import { Wrapper } from './';

describe('the Wrapper component', () => {
  it('should render properly by default', () => {
    const wrapper = shallow(<Wrapper />);
    expect(wrapper).toMatchSnapshot();
  });
});
