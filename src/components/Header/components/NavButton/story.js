import React from 'react';
import { storiesOf } from '@storybook/react';
import { MemoryRouter } from 'react-router-dom';

import NavButton from './';

storiesOf('Nav Button', module)
  .addDecorator((story) => (
    <MemoryRouter>
      <React.Fragment>{story()}</React.Fragment>
    </MemoryRouter>
  ))
  .add('in active state', () => (
    <NavButton active={true} to="/">
      Home
    </NavButton>
  ));
