export default ({ color, font, spacing }) => ({
  shutter: {
    borderBottom: `2px solid transparent`,
    borderRadius: '5px',
    color: color.black,
    display: 'inline-block',
    overflow: 'hidden',
    padding: spacing.m,
    position: 'relative',

    transition:
      'border-color 200ms ease-in-out 100ms, color 200ms ease-in-out 100ms',

    '&:before': {
      backgroundColor: color.black,
      content: '""',
      display: 'block',
      height: '0%',
      left: 0,
      position: 'absolute',
      top: 0,
      transition: 'height 200ms ease-in-out 100ms',
      width: '100%',
      zIndex: 0
    },

    '&:after': {
      backgroundColor: color.black,
      content: '""',
      display: 'block',
      height: '0%',
      left: 0,
      position: 'absolute',
      bottom: 0,
      transition: 'height 200ms ease-in-out 100ms',
      width: '100%',
      zIndex: 0
    },

    '&:hover': {
      borderColor: color.grey,
      color: color.white
    },

    '&:hover:before': {
      height: '50%'
    },

    '&:hover:after': {
      height: '50%'
    }
  },

  label: {
    position: 'relative',
    zIndex: 1
  },

  active: {
    borderBottom: `2px solid ${color.grey}`,
    fontWeight: 'bold'
  }
});
