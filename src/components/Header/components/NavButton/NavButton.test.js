import React from 'react';
import { shallow } from 'enzyme';

import { NavButton } from './';

describe('the NavButton component', () => {
  it('should render properly by default', () => {
    const wrapper = shallow(<NavButton to="/" />);
    expect(wrapper).toMatchSnapshot();
  });
});
