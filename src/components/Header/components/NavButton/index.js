// Libraries
import React from 'react';
import injectSheet from 'react-jss';
import { Link } from 'react-router-dom';

// Styles
import styles from './style';

export const NavButton = ({ classes = {}, active, to, children }) => (
  <Link to={to}>
    <div className={`${classes.shutter} ${active && classes.active}`}>
      <div className={classes.label}>{children}</div>
    </div>
  </Link>
);

export default injectSheet(styles)(NavButton);
