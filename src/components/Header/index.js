// Libraries
import React from 'react';
import injectSheet from 'react-jss';
import { Link } from 'react-router-dom';

// Styles
import styles from './style';

// Components
import NavButton from './components/NavButton';

// Images
import logo from '../../image/logo.png';

export class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { classes = {} } = this.props;

    return (
      <header className={classes.header}>
        <svg
          className={classes.backgroundShape}
          preserveAspectRatio="none"
          width="100%"
          height="100%"
          viewBox="0 0 200 100"
        >
          <polygon points="0,65 8,100 200,45 200,0" fill="#ffef03" />
        </svg>
        <div className={classes.content}>
          <Link style={{ height: '100%' }} to="/">
            <img
              className={classes.logo}
              src={logo}
              alt="Airport Currency Exchange Office"
            />
          </Link>

          <div className={classes.title}>
            Airport Currency
            <br />
            Exchange Office
          </div>

          <nav className={classes.navigation}>
            <ul>
              <li>
                <NavButton to="/">Home</NavButton>
              </li>
              <li>
                <NavButton to="/admin">Admin</NavButton>
              </li>
            </ul>
          </nav>
        </div>
      </header>
    );
  }
}

export default injectSheet(styles)(Header);
