export default ({ color, font, spacing }) => ({
  header: {
    alignItems: 'flex-start',
    background: `linear-gradient(to bottom, ${color.primary} 0%, ${
      color.primary
    } 30%, ${color.white})`,
    display: 'flex',
    fontFamily: font.family.raleway,
    fontSize: font.size.s,
    height: '7rem',
    position: 'relative'
  },

  backgroundShape: {
    height: '100%',
    position: 'absolute',
    top: 0,
    width: '100%',
    zIndex: 0
  },

  content: {
    alignItems: 'center',
    display: 'flex',
    flex: 1,
    height: '5rem',
    padding: spacing.s
  },

  logo: {
    height: '100%',
    margin: `0 ${spacing.m} 0 0`,
    position: 'relative',
    width: 'auto',
    zIndex: 1
  },

  title: {
    flex: 1,
    fontSize: font.size.xs,
    position: 'relative',
    zIndex: 1
  },

  navigation: {
    position: 'relative',
    zIndex: 1,

    '& ul': {
      display: 'flex',
      listStyle: 'none',
      margin: 0,
      padding: 0,

      '& li': {
        padding: spacing.xs,

        '& a': {
          color: color.black,
          textDecoration: 'none'
        }
      }
    }
  },

  '@media(min-width: 425px)': {
    header: {
      height: '10rem'
    },

    content: {
      height: '6rem',
      padding: spacing.m
    }
  },

  '@media(min-width: 768px)': {
    header: {
      height: '14rem'
    },

    content: {
      height: '8rem',
      padding: spacing.l
    }
  }
});
