import React from 'react';
import { shallow } from 'enzyme';

import { Header } from './';

describe('the Header component', () => {
  it('should render properly by default', () => {
    const wrapper = shallow(<Header />);
    expect(wrapper).toMatchSnapshot();
  });
});
