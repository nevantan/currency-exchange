export default ({ color, common, font, spacing }) => ({
  setting: {
    display: 'flex',
    flexDirection: 'column',
    padding: `${spacing.s} 0`,

    '& hr': {
      border: 0,
      borderTop: `1px solid ${color.silver}`,
      width: '96%'
    }
  },
  label: {
    display: 'flex',
    flexDirection: 'column',
    margin: `0 0 ${spacing.m} 0`,

    '& h2': {
      fontSize: font.size.s,
      margin: `0 0 ${spacing.s} 0`
    },
    '& small': {
      color: color.grey
    }
  },
  valueWrap: {
    alignItems: 'center',
    display: 'flex'
  },
  form: {
    alignItems: 'center',
    display: 'flex',
    flex: 1
  },
  input: {
    ...common.input,
    flex: 1,
    font: 'inherit',
    margin: `0 ${spacing.s}`
  },
  value: {
    alignItems: 'center',
    display: 'flex',
    flex: 1,
    fontFamily: font.family.sourceSans,
    letterSpacing: '1px',
    margin: `0 ${spacing.s} 0 0`
  },
  editButton: {
    ...common.iconButton
  },
  saveButton: {
    ...common.iconButton,
    margin: `0 ${spacing.s} 0 ${spacing.m}`
  },
  cancelButton: {
    ...common.iconButton,
    margin: `0 ${spacing.s} 0 0`
  }
});
