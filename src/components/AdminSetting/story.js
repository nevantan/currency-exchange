import React from 'react';
import { storiesOf } from '@storybook/react';

import AdminSetting from './';

const onEdit = () => alert('onEdit');
const onChange = () => alert('onChange');
const onSave = () => alert('onSave');
const onCancel = () => alert('onCancel');

storiesOf('Admin Setting', module)
  .add('in display state', () => (
    <AdminSetting
      editing={false}
      name="Margin Percentage"
      helptext="The percentage to add to generate the buy/sell values from the exchange quote"
      value={5}
      unit="%"
      onEdit={onEdit}
    />
  ))
  .add('in edit state', () => (
    <AdminSetting
      editing={true}
      name="Margin Percentage"
      helptext="The percentage to add to generate the buy/sell values from the exchange quote"
      value={5}
      unit="%"
      onChange={onChange}
      onSave={onSave}
      onCancel={onCancel}
    />
  ));
