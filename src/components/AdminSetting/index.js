// Libraries
import React from 'react';
import injectSheet from 'react-jss';
import FontAwesome from 'react-fontawesome';

// Styles
import styles from './style';

export const AdminSetting = ({
  classes = {},
  id,
  editing,
  helptext,
  name,
  preunit,
  postunit,
  value,
  onEdit,
  onChange,
  onSave,
  onCancel,
  ...rest
}) => (
  <div className={classes.setting}>
    <label className={classes.label} htmlFor={id}>
      <h2>{name}</h2>
      <small>{helptext}</small>
    </label>
    <div className={classes.valueWrap}>
      {editing ? ( // Setting is in "edit mode", show input
        <form className={classes.form} onSubmit={onSave}>
          <div className={classes.value}>
            {preunit}
            <input
              id={id}
              className={classes.input}
              value={value}
              onChange={onChange}
              autoFocus={true}
              {...rest}
            />
            {postunit}
          </div>
          <button className={classes.saveButton} type="submit">
            <FontAwesome name="check" />
          </button>
          <button
            className={classes.cancelButton}
            type="button"
            onClick={() => onCancel(id)}
          >
            <FontAwesome name="times" />
          </button>
        </form>
      ) : (
        // Field is in display state, show static value with edit button
        <React.Fragment>
          <div className={classes.value}>
            {preunit}
            {value}
            {postunit}
          </div>
          <div className={classes.editButton} onClick={() => onEdit(id, value)}>
            <FontAwesome name="edit" />
          </div>
        </React.Fragment>
      )}
    </div>
    <hr />
  </div>
);

export default injectSheet(styles)(AdminSetting);
