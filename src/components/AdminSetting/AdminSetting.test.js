import React from 'react';
import { shallow } from 'enzyme';

import { AdminSetting } from './';

describe('the AdminSetting component', () => {
  const classes = {
    editButton: 'edit',
    saveButton: 'save',
    cancelButton: 'cancel',
    input: 'input',
    form: 'form'
  };

  it('should render properly by default', () => {
    const wrapper = shallow(<AdminSetting />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render in display mode', () => {
    const wrapper = shallow(
      <AdminSetting
        classes={classes}
        editing={false}
        name="Test Setting"
        helptext="Lorem ipsum dolor sit amet"
        unit="%"
        value={10}
      />
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.edit').length).toBe(1);
  });

  it('should render in edit mode', () => {
    const value = 10;
    const wrapper = shallow(
      <AdminSetting
        classes={classes}
        editing={true}
        name="Test Setting"
        helptext="Lorem ipsum dolor sit amet"
        unit="%"
        value={value}
      />
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.input').length).toBe(1);
    expect(wrapper.find('.input').props().value).toBe(value);
  });

  it('should callback to switch to edit mode', () => {
    const id = 'foo';
    const value = 5;
    const onEdit = jest.fn();
    const wrapper = shallow(
      <AdminSetting
        id={id}
        classes={classes}
        editing={false}
        value={value}
        onEdit={onEdit}
      />
    );

    wrapper.find('.edit').simulate('click');

    expect(onEdit).toHaveBeenCalledTimes(1);
    expect(onEdit).toHaveBeenCalledWith(id, value);
  });

  it('should callback to update value', () => {
    const e = {
      target: {
        value: 5
      }
    };
    const onChange = jest.fn();
    const wrapper = shallow(
      <AdminSetting classes={classes} editing={true} onChange={onChange} />
    );
    wrapper.find('.input').simulate('change', e);
    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith(e);
  });

  it('should callback to save value', () => {
    const preventDefault = jest.fn();
    const onSave = jest.fn();
    const wrapper = shallow(
      <AdminSetting classes={classes} editing={true} onSave={onSave} />
    );
    wrapper.find('.form').simulate('submit', { preventDefault });
    expect(onSave).toHaveBeenCalledTimes(1);
  });

  it('should callback to revert value', () => {
    const onCancel = jest.fn();
    const wrapper = shallow(
      <AdminSetting classes={classes} editing={true} onCancel={onCancel} />
    );
    wrapper.find('.cancel').simulate('click');
    expect(onCancel).toHaveBeenCalledTimes(1);
  });
});
