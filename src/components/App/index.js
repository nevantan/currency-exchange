// Libraries
import React from 'react';
import injectSheet from 'react-jss';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

// Styles
import styles from './style';

// Components
import Header from '../Header';
import Wrapper from '../Wrapper';

// Pages
import AdminPage from '../../pages/AdminPage';
import HomePage from '../../pages/HomePage';

export const App = ({ classes = {} }) => (
  <div className={classes.app}>
    <Router>
      <React.Fragment>
        <Header />
        <Wrapper>
          <Route exact path="/" component={HomePage} />
          <Route path="/admin" component={AdminPage} />
        </Wrapper>
      </React.Fragment>
    </Router>
  </div>
);

export default injectSheet(styles)(App);
