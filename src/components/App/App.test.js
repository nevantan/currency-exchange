import React from 'react';
import { shallow } from 'enzyme';

import { App } from './';

describe('the App component', () => {
  it('renders correctly by default', () => {
    const wrapper = shallow(<App />);
    expect(wrapper).toMatchSnapshot();
  });
});
