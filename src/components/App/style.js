export default ({ color, font }) => ({
  '@global': {
    '*': {
      boxSizing: 'border-box'
    },
    html: {
      fontSize: '62.5%'
    },
    'html, body': {
      margin: 0,
      padding: 0
    },
    body: {
      backgroundColor: color.white,
      color: color.black,
      fontFamily: font.family.raleway,
      fontSize: font.size.s
    }
  }
});
