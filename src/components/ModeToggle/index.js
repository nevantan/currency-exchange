// Libraries
import React from 'react';
import injectSheet from 'react-jss';

// Styles
import styles from './style';

export const ModeToggle = ({
  active = 0,
  buttons = [],
  classes = {},
  className,
  onChange
}) => (
  <div className={`${classes.toggle} ${className}`}>
    {buttons.map(({ id, label }, i) => (
      <button
        id={id}
        key={label}
        className={`${classes.button} ${i === active && 'active'}`}
        onClick={() => onChange(id)}
      >
        {label}
      </button>
    ))}
  </div>
);

export default injectSheet(styles)(ModeToggle);
