export default ({ color, font, spacing }) => ({
  toggle: {
    border: `1px solid ${color.silver}`,
    borderRadius: '2px',
    boxShadow: '1px 1px 1px 1px rgba(0, 0, 0, 0.05) inset',
    display: 'flex',
    fontSize: font.size.s,
    overflow: 'hidden',

    '& button': {
      backgroundColor: 'transparent',
      border: 0,
      boxShadow: '1px 0px 1px 1px rgba(0, 0, 0, 0.05)',
      cursor: 'pointer',
      flex: 1,
      font: 'inherit',
      padding: spacing.s
    },

    '& button.active': {
      backgroundColor: color.primary
    }
  }
});
