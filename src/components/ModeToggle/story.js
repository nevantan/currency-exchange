import React from 'react';
import { storiesOf } from '@storybook/react';

import ModeToggle from './';

class ToggleWrap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 0
    };
  }

  render() {
    return (
      <ModeToggle
        labels={['Buy', 'Sell']}
        active={this.state.active}
        onChange={(index) => this.setState({ active: index })}
      />
    );
  }
}

storiesOf('Mode Toggle', module)
  .add('with first option active', () => (
    <ModeToggle labels={['Buy', 'Sell']} active={0} onChange={() => {}} />
  ))
  .add('with second option active', () => (
    <ModeToggle labels={['Buy', 'Sell']} active={1} onChange={() => {}} />
  ))
  .add('with animated transitions', () => <ToggleWrap />);
