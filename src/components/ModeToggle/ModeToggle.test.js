import React from 'react';
import { shallow } from 'enzyme';

import { ModeToggle } from './';

describe('the ModeToggle component', () => {
  const classes = {
    button: 'button'
  };
  const buttons = [
    { id: 'buy', label: 'Buy EUR' },
    { id: 'sell', label: 'Sell EUR' }
  ];

  it('should render properly by default', () => {
    const wrapper = shallow(<ModeToggle />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render multiple buttons', () => {
    const wrapper = shallow(
      <ModeToggle active={0} buttons={buttons} classes={classes} />
    );
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.button').length).toBe(2);
  });

  it('should callback to switch active on click', () => {
    const onChange = jest.fn();
    const wrapper = shallow(
      <ModeToggle
        active={0}
        buttons={buttons}
        classes={classes}
        onChange={onChange}
      />
    );

    wrapper.find('#buy').simulate('click');

    expect(onChange).toHaveBeenCalledTimes(1);
    expect(onChange).toHaveBeenCalledWith('buy');
  });
});
