import React from 'react';
import { storiesOf } from '@storybook/react';

import BuySellModal from './';

const currency = {
  id: 'EUR',
  symbol: '\u20ac',
  rate: 1.13,
  buy: 1.1865,
  sell: 1.0735
};

class ModalWrap extends React.Component {
  constructor(props) {
    super(props);

    this.state = { isOpen: true };
  }

  render() {
    return (
      <React.Fragment>
        <button onClick={() => this.setState({ isOpen: true })}>
          Open Modal
        </button>
        <BuySellModal
          {...this.props}
          isOpen={this.state.isOpen}
          closeModal={() => this.setState({ isOpen: false })}
        />
      </React.Fragment>
    );
  }
}

storiesOf('Buy-Sell Modal', module)
  .add('in buy mode', () => (
    <ModalWrap action="buy" currency={currency} isOpen={true} />
  ))
  .add('in sell mode', () => (
    <ModalWrap action="sell" currency={currency} isOpen={true} />
  ));
