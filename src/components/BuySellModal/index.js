// Libraries
import React from 'react';
import injectSheet from 'react-jss';
import { connect } from 'react-redux';
import Modal from 'react-modal';

// Styles
import styles from './style';

// Components
import ModeToggle from '../ModeToggle';

// Actions
import {
  buyCurrencyAction,
  sellCurrencyAction
} from '../../redux/actions/currency';
import { setModalOpen } from '../../redux/actions/configuration';

export class BuySellModal extends React.Component {
  constructor(props) {
    super(props);

    this.calcValues = this.calcValues.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleToggle = this.handleToggle.bind(this);
    this.handleTransaction = this.handleTransaction.bind(this);

    const initialValues = this.calcValues(100, props.currency[props.action]);

    this.state = {
      action: props.action,
      ...initialValues
    };
  }

  calcValues(value, rate) {
    const { commissionPct, surcharge, minCommission } = this.props;

    const subtotal = value * rate;
    const commission = Math.max(
      subtotal * commissionPct + surcharge,
      minCommission
    );
    const total = subtotal + commission;

    return {
      value,
      subtotal,
      commission,
      total
    };
  }

  handleToggle(action) {
    const values = this.calcValues(
      this.state.value,
      this.props.currency[action]
    );

    this.setState({
      action,
      ...values
    });
  }

  handleChange(e) {
    const rate = this.props.currency[this.state.action];
    this.setState(this.calcValues(Math.abs(e.target.value), rate));
  }

  handleTransaction(e) {
    e.preventDefault();

    if (this.state.action === 'buy') {
      this.props.buyCurrency(this.props.currency.id, this.state.value);
    } else {
      this.props.sellCurrency(this.props.currency.id, this.state.value);
    }

    this.props.closeModal();
  }

  render() {
    const { classes = {}, isOpen, closeModal, currency } = this.props;

    return (
      <Modal
        isOpen={isOpen}
        onRequestClose={closeModal}
        contentLabel="Buy/Sell Currency Popup"
        className={classes.modal}
        overlayClassName={classes.overlay}
        appElement={document.getElementById('app')}
      >
        <ModeToggle
          active={this.state.action === 'buy' ? 0 : 1}
          buttons={[
            { id: 'buy', label: `Buy ${currency.id}` },
            { id: 'sell', label: `Sell ${currency.id}` }
          ]}
          className={classes.toggle}
          onChange={this.handleToggle}
        />
        <form id="buySellForm" onSubmit={this.handleTransaction}>
          <div className={classes.currencyInput}>
            {currency.symbol}
            <input
              id="amount"
              type="number"
              min="0"
              value={this.state.value}
              onChange={this.handleChange}
            />
            .00
          </div>

          <div className={classes.labelGroup}>
            <div className={classes.label}>Exchange rate</div>
            <div className={classes.value}>
              {currency[this.state.action].toFixed(2)}
            </div>
          </div>

          <div className={classes.labelGroup}>
            <div className={classes.label}>Subtotal</div>
            <div className={classes.value}>
              ${this.state.subtotal.toFixed(2)}
            </div>
          </div>

          <div className={classes.labelGroup}>
            <div className={classes.label}>Commission</div>
            <div className={classes.value}>
              ${this.state.commission.toFixed(2)}
            </div>
          </div>

          <hr />

          <div className={classes.labelGroup}>
            <div className={classes.label}>Total</div>
            <div className={classes.value}>${this.state.total.toFixed(2)}</div>
          </div>

          <div className={classes.buttonWrap}>
            <button
              id="cancelButton"
              className={classes.secondaryButton}
              onClick={closeModal}
              type="button"
            >
              Cancel
            </button>
            <button
              id="transactionButton"
              className={classes.primaryButton}
              type="submit"
            >
              {this.state.action === 'buy'
                ? `Buy ${currency.id}`
                : `Sell ${currency.id}`}
            </button>
          </div>
        </form>
      </Modal>
    );
  }
}

export const StyledBuySellModal = injectSheet(styles)(BuySellModal);

export const mapStateToProps = ({ currency, configuration }, ownProps) => ({
  action: configuration.modal.action,
  isOpen:
    ownProps && ownProps.isOpen !== undefined
      ? ownProps.isOpen
      : configuration.modal.isOpen,
  commissionPct: configuration.commissionPct,
  currency: currency.filter(({ id }) => id === configuration.modal.currency)[0],
  surcharge: configuration.surcharge,
  minCommission: configuration.minCommission
});

export const mapDispatchToProps = (dispatch) => ({
  buyCurrency: (id, amount) => dispatch(buyCurrencyAction(id, amount)),
  sellCurrency: (id, amount) => dispatch(sellCurrencyAction(id, amount)),
  closeModal: () => dispatch(setModalOpen('', ''))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StyledBuySellModal);
