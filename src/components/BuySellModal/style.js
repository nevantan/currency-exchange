import c from 'color';

export default ({ color, common, font, spacing }) => ({
  ...common,
  modal: {
    backgroundColor: color.white,
    border: `1px solid ${color.silver}`,
    borderRadius: '2px',
    boxShadow: `1px 1px 1px 1px rgba(0, 0, 0, 0.05)`,
    maxWidth: '40rem',
    padding: spacing.m
  },
  overlay: {
    alignItems: 'flex-start',
    backgroundColor: 'rgba(0, 0, 0, 0.1)',
    display: 'flex',
    height: '100%',
    justifyContent: 'center',
    left: 0,
    paddingTop: '5%',
    position: 'fixed',
    top: 0,
    width: '100%'
  },
  toggle: {
    marginBottom: spacing.s
  },
  currencyInput: {
    alignItems: 'center',
    borderTop: `1px solid ${color.grey}`,
    borderBottom: `1px solid ${color.grey}`,
    display: 'flex',
    fontSize: font.size.m,
    marginBottom: spacing.s,
    padding: `${spacing.s} 0`,

    '& input': {
      border: `1px solid #ddd`,
      borderRadius: '1px',
      font: 'inherit',
      flex: 1,
      margin: `0 ${spacing.s}`,
      padding: spacing.xs,
      textAlign: 'right'
    }
  },
  labelGroup: {
    display: 'flex',
    justifyContent: 'space-between',
    padding: `${spacing.s} 0`
  },
  label: {
    fontWeight: 'bold'
  },
  buttonWrap: {
    display: 'flex',
    justifyContent: 'flex-end',
    marginTop: spacing.m
  },
  primaryButton: {
    ...common.button,
    ...common.primaryButton,
    marginLeft: spacing.s
  }
});
