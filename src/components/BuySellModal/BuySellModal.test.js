import React from 'react';
import { shallow } from 'enzyme';

import { BuySellModal, mapStateToProps, mapDispatchToProps } from './';

import {
  buyCurrencyAction,
  sellCurrencyAction
} from '../../redux/actions/currency';

describe('the BuySellModal component', () => {
  const currency = { id: 'EUR', buy: 1.14, sell: 1.12 };

  it('should render properly by default', () => {
    const wrapper = shallow(<BuySellModal action="buy" currency={currency} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should calculate display values correctly', () => {
    const wrapper = shallow(
      <BuySellModal
        action="buy"
        currency={currency}
        commissionPct={0.029}
        surcharge={0.3}
        minCommission={2}
      />
    );

    const {
      value,
      subtotal,
      commission,
      total
    } = wrapper.instance().calcValues(100, currency.buy);
    expect(value).toBe(100);
    expect(subtotal).toBeCloseTo(114, 4);
    expect(commission).toBeCloseTo(3.606, 4);
    expect(total).toBeCloseTo(117.606, 4);
  });

  it('should handle the input value changing', () => {
    const wrapper = shallow(
      <BuySellModal
        action="buy"
        currency={currency}
        commissionPct={0.029}
        surcharge={0.3}
        minCommission={2}
      />
    );
    wrapper.find('#amount').simulate('change', { target: { value: 1 } });

    expect(wrapper.state('value')).toBe(1);
  });

  it('should handle the action toggle state properly', () => {
    const wrapper = shallow(
      <BuySellModal
        action="buy"
        currency={currency}
        commissionPct={0.029}
        surcharge={0.3}
        minCommission={2}
      />
    );
    wrapper.instance().handleToggle('sell');
    expect(wrapper.state('action')).toBe('sell');
  });

  it('should dispatch a buy transaction properly', () => {
    const buyCurrency = jest.fn();
    const preventDefault = jest.fn();
    const closeModal = jest.fn();

    const wrapper = shallow(
      <BuySellModal
        action="buy"
        currency={currency}
        buyCurrency={buyCurrency}
        closeModal={closeModal}
      />
    );

    wrapper.find('#buySellForm').simulate('submit', { preventDefault });

    expect(preventDefault).toHaveBeenCalledTimes(1);
    expect(buyCurrency).toHaveBeenCalledTimes(1);
    expect(buyCurrency).toHaveBeenCalledWith('EUR', 100);
    expect(closeModal).toHaveBeenCalledTimes(1);
  });

  it('should dispatch a sell transaction properly', () => {
    const sellCurrency = jest.fn();
    const preventDefault = jest.fn();
    const closeModal = jest.fn();

    const wrapper = shallow(
      <BuySellModal
        action="sell"
        currency={currency}
        sellCurrency={sellCurrency}
        closeModal={closeModal}
      />
    );

    wrapper.find('#buySellForm').simulate('submit', { preventDefault });

    expect(preventDefault).toHaveBeenCalledTimes(1);
    expect(sellCurrency).toHaveBeenCalledTimes(1);
    expect(sellCurrency).toHaveBeenCalledWith('EUR', 100);
    expect(closeModal).toHaveBeenCalledTimes(1);
  });

  it('maps state to props', () => {
    const commissionPct = 0.1;
    const surcharge = 0.3;
    const minCommission = 1;
    const modal = {
      isOpen: true,
      currency: 'EUR',
      action: 'buy'
    };
    const state = {
      currency: [currency],
      configuration: {
        margin: 0.05,
        commissionPct,
        surcharge,
        minCommission,
        modal
      }
    };

    const props = mapStateToProps(state);
    expect(props).toEqual({
      commissionPct,
      surcharge,
      minCommission,
      action: modal.action,
      isOpen: modal.isOpen,
      currency
    });
  });

  it('maps dispatch to props', () => {
    const amount = 1;
    const dispatch = jest.fn();
    const props = mapDispatchToProps(dispatch);

    props.buyCurrency('EUR', amount);
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch).toHaveBeenCalledWith(buyCurrencyAction('EUR', amount));

    props.sellCurrency('EUR', amount);
    expect(dispatch).toHaveBeenCalledTimes(2);
    expect(dispatch).toHaveBeenCalledWith(sellCurrencyAction('EUR', amount));
  });
});
