import React from 'react';
import { storiesOf } from '@storybook/react';

import ConnectedCurrencyTable, {
  StyledCurrencyTable as CurrencyTable
} from './';

const currencies = [
  { id: 'EUR', stock: 1000, buy: 1.19, sell: 1.07, low: false },
  { id: 'CAD', stock: 1200, buy: 0.79, sell: 0.72, low: false },
  { id: 'GBP', stock: 300, buy: 1.35, sell: 1.23, low: false }
];

storiesOf('Currency Table', module)
  .add('with single currency', () => (
    <CurrencyTable currencies={[currencies[0]]} />
  ))
  .add('with multiple currencies', () => (
    <CurrencyTable currencies={currencies} />
  ))
  .add('sorted by Currency, asc', () => (
    <CurrencyTable currencies={[]} sortby="currency" sortdir="asc" />
  ))
  .add('sorted by Stock, desc', () => (
    <CurrencyTable currencies={[]} sortby="stock" sortdir="desc" />
  ))
  .add('with redux', () => <ConnectedCurrencyTable />);
