export default ({ color, font, spacing }) => ({
  table: {
    borderCollapse: 'collapse',
    width: '100%'
  },
  headRow: {
    borderTop: `1px solid ${color.grey}`,
    borderBottom: `1px solid ${color.grey}`
  },
  currency: {
    textAlign: 'left'
  },
  number: {
    textAlign: 'right'
  },
  info: {
    color: color.grey,
    display: 'flex',
    justifyContent: 'space-between',
    marginBottom: spacing.s
  }
});
