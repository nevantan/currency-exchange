export default ({ color, spacing }) => ({
  row: {
    border: `1px solid transparent`,
    borderBottom: `1px solid ${color.silver}`,
    transition: 'background-color 300ms ease-in-out',

    '& td': {
      padding: `${spacing.m} ${spacing.s}`
    },

    '&:last-child': {
      border: 0
    },

    '&::after': {
      boxShadow: '2px 2px 2px rgba(0, 0, 0, 0.1)',
      opacity: 0,
      transition: 'opacity 300ms ease-in-out'
    },

    '&:hover': {
      backgroundColor: color.offwhite
    }
  },
  number: {
    textAlign: 'right'
  },
  low: {
    color: color.red
  },
  currency: {},
  stock: {},
  buy: {},
  sell: {},
  currencyButton: {
    backgroundColor: 'transparent',
    border: `1px solid transparent`,
    borderRadius: '2px',
    cursor: 'pointer',
    padding: `${spacing.s}`,
    textDecoration: 'underline'
  }
});
