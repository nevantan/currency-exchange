import React from 'react';
import { storiesOf } from '@storybook/react';

import CurrencyRow from './';

storiesOf('Currency Row', module)
  .addDecorator((story) => (
    <table style={{ width: '100%', borderCollapse: 'collapse' }}>
      <thead>
        <tr>
          <th>Currency</th>
          <th>Stock</th>
          <th>Buy</th>
          <th>Sell</th>
        </tr>
      </thead>
      <tbody>{story()}</tbody>
    </table>
  ))
  .add('with well-stocked currency', () => (
    <CurrencyRow id="EUR" stock={1000} buy={1.19} sell={1.07} />
  ))
  .add('with low-stock currency', () => (
    <CurrencyRow id="EUR" stock={200} buy={1.19} sell={1.07} low={true} />
  ));
