import React from 'react';
import { shallow } from 'enzyme';

import { CurrencyRow } from './';

describe('the CurrencyRow component', () => {
  const classes = {
    row: 'row',
    currency: 'currency',
    stock: 'stock',
    number: 'number',
    low: 'low',
    buy: 'buy',
    sell: 'sell'
  };

  it('should render properly by default', () => {
    const wrapper = shallow(<CurrencyRow />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render low-stock currency', () => {
    const wrapper = shallow(
      <CurrencyRow stock="200" low={true} classes={classes} />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
