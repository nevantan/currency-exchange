// Libraries
import React from 'react';
import injectSheet from 'react-jss';
import { connect } from 'react-redux';

// Styles
import styles from './style';

// Actions
import { setModalOpen } from '../../../../redux/actions/configuration';

export const CurrencyRow = ({
  id,
  amount = 0,
  buy = 0,
  sell = 0,
  low = false,
  classes = {},
  openModal
}) => (
  <tr id={id} className={classes.row}>
    <td className={`${classes.currency}`}>{id}</td>
    <td
      className={`${classes.stock} ${classes.number} ${low ? classes.low : ''}`}
    >
      {amount}
    </td>
    <td className={`${classes.buy} ${classes.number}`}>
      <button
        className={classes.currencyButton}
        onClick={() => openModal('buy')}
      >
        {buy.toFixed(4)}
      </button>
    </td>
    <td className={`${classes.sell} ${classes.number}`}>
      <button
        className={classes.currencyButton}
        onClick={() => openModal('sell')}
      >
        {sell.toFixed(4)}
      </button>
    </td>
  </tr>
);

export const StyledCurrencyRow = injectSheet(styles)(CurrencyRow);

const mapDispatchtoProps = (dispatch, { id }) => ({
  openModal: (action) => dispatch(setModalOpen(action, id))
});

export default connect(
  undefined,
  mapDispatchtoProps
)(StyledCurrencyRow);
