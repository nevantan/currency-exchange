import React from 'react';
import { storiesOf } from '@storybook/react';

import LastUpdated from './';

storiesOf('Last Updated', module).add('with provided date', () => (
  <LastUpdated time={+new Date()} />
));
