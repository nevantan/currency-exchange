// Libraries
import React from 'react';
import injectSheet from 'react-jss';
import moment from 'moment';

// Styles
import styles from './style';

export class LastUpdated extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      time: moment(props.time).fromNow()
    };
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      this.setState({
        time: moment(this.props.time).fromNow()
      });
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    const { classes = {}, ...props } = this.props;

    return <div {...props}>Last updated {this.state.time}</div>;
  }
}

export default injectSheet(styles)(LastUpdated);
