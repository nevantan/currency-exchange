import React from 'react';
import { shallow } from 'enzyme';

import { LastUpdated } from './';

describe('the LastUpdated component', () => {
  it('should render properly by default', () => {
    const wrapper = shallow(<LastUpdated />);
    expect(wrapper).toMatchSnapshot();
  });
});
