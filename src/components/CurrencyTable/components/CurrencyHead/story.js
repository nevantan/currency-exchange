import React from 'react';
import { storiesOf } from '@storybook/react';

import CurrencyHead from './';

storiesOf('Currency Head', module)
  .addDecorator((story) => (
    <table style={{ width: '100%', borderCollapse: 'collapse' }}>
      <thead>
        <tr>{story()}</tr>
      </thead>
    </table>
  ))
  .add('unsorted', () => (
    <CurrencyHead name="Currency" align="left" sort={false} />
  ))
  .add('sort ascending', () => (
    <CurrencyHead name="Currency" align="left" sort={'asc'} />
  ))
  .add('sort descending', () => (
    <CurrencyHead name="Currency" align="left" sort={'desc'} />
  ))
  .add('align right', () => (
    <CurrencyHead name="Currency" align="right" sort={false} />
  ));
