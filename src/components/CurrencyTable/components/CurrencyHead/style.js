export default ({ color, font, spacing }) => ({
  head: ({ align }) => ({
    cursor: 'pointer',
    padding: spacing.s,
    textAlign: align
  })
});
