// Libraries
import React from 'react';
import injectSheet from 'react-jss';
import FontAwesome from 'react-fontawesome';

// Styles
import styles from './style';

// sort ? sort === 'asc' ? 'sort-up' : 'sort-down' : 'sort'
const icon = (sort) => {
  if (sort === 'asc') return 'sort-up';
  if (sort === 'desc') return 'sort-down';
  return 'sort';
};

export const CurrencyHead = ({ name, align, sort, classes = {} }) => (
  <th className={classes.head}>
    {name} <FontAwesome name={icon(sort)} />
  </th>
);

export default injectSheet(styles)(CurrencyHead);
