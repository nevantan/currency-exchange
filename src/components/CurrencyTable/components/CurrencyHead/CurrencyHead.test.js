import React from 'react';
import { shallow } from 'enzyme';

import { CurrencyHead } from './';

describe('the CurrencyHead component', () => {
  it('should render properly by default', () => {
    const wrapper = shallow(<CurrencyHead />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render sort asc', () => {
    const wrapper = shallow(
      <CurrencyHead name="Currency" align="left" sort="asc" />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('should render sort desc', () => {
    const wrapper = shallow(
      <CurrencyHead name="Currency" align="left" sort="desc" />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
