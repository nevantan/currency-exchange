// Libraries
import React from 'react';
import injectSheet from 'react-jss';
import FontAwesome from 'react-fontawesome';
import { connect } from 'react-redux';
import moment from 'moment';

// Styles
import styles from './style';

// Components
import CurrencyHead from './components/CurrencyHead';
import CurrencyRow from './components/CurrencyRow';
import LastUpdated from './components/LastUpdated';

export class CurrencyTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const {
      classes = {},
      currencies = [],
      sortby = '',
      sortdir = 'asc',
      lastUpdate,
      baseCurrency = {
        id: '',
        symbol: '',
        amount: 0
      }
    } = this.props;

    return (
      <React.Fragment>
        <div className={classes.info}>
          <div className={classes.baseCurrency}>
            {baseCurrency.id}: {baseCurrency.symbol}
            {baseCurrency.amount.toFixed(2)}
          </div>
          <LastUpdated className={classes.lastUpdate} time={lastUpdate} />
        </div>
        <table className={classes.table}>
          <thead>
            <tr className={classes.headRow}>
              <CurrencyHead
                name="Currency"
                align="left"
                sort={sortby === 'currency' ? sortdir : false}
              />
              <CurrencyHead
                name="Stock"
                align="right"
                sort={sortby === 'stock' ? sortdir : false}
              />
              <CurrencyHead
                name="Buy"
                align="right"
                sort={sortby === 'buy' ? sortdir : false}
              />
              <CurrencyHead
                name="Sell"
                align="right"
                sort={sortby === 'sell' ? sortdir : false}
              />
            </tr>
          </thead>
          <tbody>
            {currencies.map((currency) => (
              <CurrencyRow
                key={currency.id}
                {...currency}
                low={currency.amount <= currency.initial / 4}
              />
            ))}
          </tbody>
        </table>
      </React.Fragment>
    );
  }
}

export const StyledCurrencyTable = injectSheet(styles)(CurrencyTable);

export const mapStateToProps = (state) => ({
  currencies: state.currency.filter(
    (currency) => currency.id !== state.configuration.baseCurrency
  ),
  baseCurrency: state.currency.filter(
    (currency) => currency.id === state.configuration.baseCurrency
  )[0],
  lastUpdate: state.configuration.lastUpdate
});

export default connect(mapStateToProps)(StyledCurrencyTable);
