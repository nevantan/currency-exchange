import React from 'react';
import { shallow } from 'enzyme';

import { CurrencyTable, mapStateToProps } from './';

describe('the CurrencyTable component', () => {
  const currencies = [
    { id: 'EUR', stock: 1000, buy: 1.19, sell: 1.07, low: false },
    { id: 'CAD', stock: 1200, buy: 0.79, sell: 0.72, low: false },
    { id: 'GBP', stock: 300, buy: 1.35, sell: 1.23, low: false }
  ];

  it('should render properly by default', () => {
    const wrapper = shallow(<CurrencyTable />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render currency rows', () => {
    const wrapper = shallow(<CurrencyTable currencies={currencies} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render the various sort possibilities', () => {
    // Sort by currency
    let wrapper = shallow(<CurrencyTable sortby="currency" sortdir="asc" />);
    expect(wrapper).toMatchSnapshot();

    // Sort by stock
    wrapper = shallow(<CurrencyTable sortby="stock" sortdir="asc" />);
    expect(wrapper).toMatchSnapshot();

    // Sort by buy
    wrapper = shallow(<CurrencyTable sortby="buy" sortdir="asc" />);
    expect(wrapper).toMatchSnapshot();

    // Sort by sell
    wrapper = shallow(<CurrencyTable sortby="sell" sortdir="asc" />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should map state to props', () => {
    const state = {
      currency: currencies,
      configuration: {
        lastUpdate: 0
      }
    };
    const props = mapStateToProps(state);

    expect(props).toEqual({
      currencies,
      lastUpdate: 0
    });
  });
});
