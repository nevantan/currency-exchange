// Libraries
import React from 'react';
import injectSheet from 'react-jss';
import { connect } from 'react-redux';

// Styles
import styles from './style';

// Components
import BuySellModal from '../../components/BuySellModal';
import CurrencyTable from '../../components/CurrencyTable';

export class HomePage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { classes = {}, modal = {} } = this.props;

    return (
      <div className={classes.page}>
        <h1>Home</h1>
        <CurrencyTable />
        {modal.isOpen && <BuySellModal />}
      </div>
    );
  }
}

export const StyledHomePage = injectSheet(styles)(HomePage);

const mapStateToProps = ({ configuration }) => ({
  modal: configuration.modal
});

export default connect(mapStateToProps)(StyledHomePage);
