import React from 'react';
import { shallow } from 'enzyme';

import { HomePage } from './';

describe('the HomePage component', () => {
  it('should render properly by default', () => {
    const wrapper = shallow(<HomePage />);
    expect(wrapper).toMatchSnapshot();
  });
});
