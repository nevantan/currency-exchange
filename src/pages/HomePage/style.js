export default ({ color, font, spacing }) => ({
  page: {
    padding: `0 ${spacing.m}`
  }
});
