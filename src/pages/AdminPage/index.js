// Libraries
import React from 'react';
import injectSheet from 'react-jss';
import { connect } from 'react-redux';

// Styles
import styles from './style';

// Components
import AdminSetting from '../../components/AdminSetting';

// Actions
import {
  setMargin,
  setCommissionPct,
  setSurcharge,
  setMinCommission,
  setRefreshRate
} from '../../redux/actions/configuration';

export class AdminPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      editing: '',
      value: ''
    };

    this.onEdit = this.onEdit.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  onEdit(id, value) {
    this.setState((prev) => {
      // If nothing else is being edited, update state
      if (prev.editing === '') return { editing: id, value: value };

      // Else abort edit
      return prev;
    });
  }

  onChange(e) {
    this.setState({
      value: e.target.value * 1 // Convert to int
    });
  }

  onSave(e, cb) {
    // Prevent page refresh
    e.preventDefault();

    // Call the setting-specific callback/save function
    cb();

    // Reset component to display mode
    this.setState({
      editing: '',
      value: ''
    });
  }

  onCancel(id) {
    this.setState((prev) => {
      // If the field currently being edited is the one that called cancel, update state
      if (prev.editing === id) return { editing: '', value: '' };

      // Else abort cancel
      return prev;
    });
  }

  render() {
    const {
      classes = {},
      margin,
      commissionPct,
      surcharge,
      minCommission,
      refreshRate,
      saveMargin,
      saveCommissionPct,
      saveSurcharge,
      saveMinCommission,
      saveRefreshRate
    } = this.props;

    // Extract out dependent props
    const settings = [
      {
        id: 'margin',
        name: 'Margin Percentage',
        type: 'number',
        helptext:
          'The percentage to add to generate the buy/sell values from the exchange quote',
        value: margin,
        postunit: '%',
        onSave: saveMargin(this.state.value)
      },
      {
        id: 'commissionPct',
        name: 'Commission Percentage',
        type: 'number',
        helptext: 'The percentage of the transaction to add on as a commission',
        value: commissionPct,
        postunit: '%',
        onSave: saveCommissionPct(this.state.value)
      },
      {
        id: 'surcharge',
        name: 'Surcharge',
        type: 'number',
        step: 'any',
        helptext: 'The flat rate to add on to the transaction as a commission',
        value: surcharge,
        preunit: '$',
        onSave: saveSurcharge(this.state.value)
      },
      {
        id: 'minCommission',
        name: 'Minimum Commission',
        type: 'number',
        step: 'any',
        helptext:
          'At least this much will be charged even if the value calculated from Commission Percentage plus Surcharge is lower',
        value: minCommission,
        preunit: '$',
        onSave: saveMinCommission(this.state.value)
      },
      {
        id: 'refreshRate',
        name: 'Refresh Rate',
        type: 'number',
        helptext:
          'How often to refetch new exchange rates from the currency exchange',
        value: refreshRate,
        postunit: ' seconds',
        onSave: saveRefreshRate(this.state.value)
      }
    ];

    return (
      <div className={classes.page}>
        <h1>Admin Settings</h1>
        {settings.map((setting) => {
          // Is this setting in edit mode?
          let inEditMode = false;
          if (this.state.editing === setting.id) inEditMode = true;

          // Default to static value
          let value = setting.value;

          // If this setting is in edit mode, use the temp state value instead
          if (inEditMode) value = this.state.value;

          // Combine extracted props (...setting), calculated values, and common props
          return (
            <AdminSetting
              {...setting}
              value={value}
              key={setting.id}
              editing={inEditMode}
              onEdit={this.onEdit}
              onChange={this.onChange}
              onSave={(e) => this.onSave(e, setting.onSave)}
              onCancel={this.onCancel}
            />
          );
        })}
      </div>
    );
  }
}

export const StyledAdminPage = injectSheet(styles)(AdminPage);

export const mapStateToProps = (state) => ({
  margin: Math.floor(state.configuration.margin * 100), // Decimal to percent (ie. 0.05 -> 5%)
  commissionPct: Math.floor(state.configuration.commissionPct * 100), // Decimal to percent
  surcharge: state.configuration.surcharge.toFixed(2), // Money, force two decimals
  minCommission: state.configuration.minCommission.toFixed(2), // Money, force two decimals
  refreshRate: Math.floor(state.configuration.refreshRate / 1000) // Milliseconds to seconds
});

export const mapDispatchToProps = (dispatch) => ({
  saveMargin: (value) => () => dispatch(setMargin(value / 100)), // Percent to decimal (ie. 5% -> 0.05)
  saveCommissionPct: (value) => () => dispatch(setCommissionPct(value / 100)), // Percent to decimal
  saveSurcharge: (value) => () => dispatch(setSurcharge(value)),
  saveMinCommission: (value) => () => dispatch(setMinCommission(value)),
  saveRefreshRate: (value) => () => dispatch(setRefreshRate(value * 1000)) // Seconds to milliseconds
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StyledAdminPage);
