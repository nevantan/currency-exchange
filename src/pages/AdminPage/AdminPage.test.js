import React from 'react';
import { shallow } from 'enzyme';

import { AdminPage, mapStateToProps } from './';

describe('the AdminPage component', () => {
  const saveMargin = jest.fn();
  const saveCommissionPct = jest.fn();
  const saveSurcharge = jest.fn();
  const saveMinCommission = jest.fn();
  const saveRefreshRate = jest.fn();

  const dispatch = {
    saveMargin: (value) => () => saveMargin(value),
    saveCommissionPct: (value) => () => saveCommissionPct(value),
    saveSurcharge: (value) => () => saveSurcharge(value),
    saveMinCommission: (value) => () => saveMinCommission(value),
    saveRefreshRate: (value) => () => saveRefreshRate(value)
  };

  it('should render properly by default', () => {
    const wrapper = shallow(<AdminPage {...dispatch} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render a setting in edit mode', () => {
    const wrapper = shallow(<AdminPage {...dispatch} />);
    wrapper.setState({
      editing: 'margin',
      value: 5
    });

    expect(wrapper).toMatchSnapshot();
  });

  describe('the AdminSetting callbacks', () => {
    it('should initiate editing', () => {
      const id = 'foo';
      const value = 5;
      const wrapper = shallow(<AdminPage {...dispatch} />);

      // Call onEdit callback and update wrapper state
      wrapper.instance().onEdit(id, value);
      wrapper.update();

      expect(wrapper.state('editing')).toBe(id);
      expect(wrapper.state('value')).toBe(value);
    });

    it('should not initiate editing twice', () => {
      const id = 'foo';
      const value = 5;
      const wrapper = shallow(<AdminPage {...dispatch} />);

      // Call onEdit callback twice
      wrapper.instance().onEdit(id, value);
      wrapper.update();

      wrapper.instance().onEdit('bar', '');
      wrapper.update();

      // Expect values from first call as the second should have failed out
      expect(wrapper.state('editing')).toBe(id);
      expect(wrapper.state('value')).toBe(value);
    });

    it('should update the value', () => {
      const value = 5;
      const wrapper = shallow(<AdminPage {...dispatch} />);

      wrapper.instance().onChange({
        target: {
          value
        }
      });
      wrapper.update();

      expect(wrapper.state('value')).toBe(value);
    });

    it('should cancel editing', () => {
      const id = 'foo';
      const wrapper = shallow(<AdminPage {...dispatch} />);

      wrapper.setState({
        editing: id
      });

      wrapper.instance().onCancel(id);
      wrapper.update();

      expect(wrapper.state('editing')).toBe('');
    });

    it('should not cancel editing if id does not match', () => {
      const id = 'foo';
      const wrapper = shallow(<AdminPage {...dispatch} />);

      wrapper.setState({
        editing: id
      });

      wrapper.instance().onCancel('bar');
      wrapper.update();

      expect(wrapper.state('editing')).toBe(id);
    });
  });

  describe('the redux connection', () => {
    it('maps state to props', () => {
      const state = {
        configuration: {
          margin: 0.05,
          commissionPct: 0.1,
          surcharge: 0.3,
          minCommission: 1,
          refreshRate: 10000
        }
      };

      expect(mapStateToProps(state)).toEqual({
        margin: 5, // Converted decimal to percent (0.05 -> 5%)
        commissionPct: 10, // Converted decimal to percent (0.1 -> 10%)
        surcharge: '0.30', // Forced to two decimal places ($0.30)
        minCommission: '1.00', // Forced to two decimal places ($1.00)
        refreshRate: 10 // Converted milliseconds to seconds (10000 -> 10)
      });
    });
  });
});
