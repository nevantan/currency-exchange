import puppeteer from 'puppeteer';

describe('as a kiosk clerk', async () => {
  let browser, page;

  beforeEach(async () => {
    browser = await puppeteer.launch();
    page = await browser.newPage();
  });

  it('I want to enter a purchase of 150 EUR into the system', async () => {
    await page.goto(process.env.TEST_ENDPOINT);

    // Click buy button
    await page.click('#EUR td:nth-of-type(3) button');

    // Clear field and enter new value
    const valueInput = await page.$('#amount');
    await valueInput.click({ clickCount: 3 }); // Triple click to select contents of input
    await valueInput.type('150');
    await valueInput.dispose();

    // Click 'Buy' button
    await page.click('#transactionButton');

    // Read current stock after purchase
    const stockCell = await page.$('#EUR td:nth-of-type(2)');
    const stock = await page.evaluate(
      (el) => parseInt(el.textContent),
      stockCell
    );

    // Assert value
    expect(stock).toBe(850);
  });

  it('I want to cancel entering a purchase', async () => {
    await page.goto(process.env.TEST_ENDPOINT);

    // Click buy button
    await page.click('#EUR td:nth-of-type(3) button');

    // Click cancel button
    await page.click('#cancelButton');

    expect((await page.$$('#buySellForm')).length).toBe(0);
  });

  it('I want to be prevented from making negative purchases', async () => {
    await page.goto(process.env.TEST_ENDPOINT);

    // Click buy button
    await page.click('#EUR td:nth-of-type(3) button');

    // Clear field and enter new negative value
    const valueInput = await page.$('#amount');
    await valueInput.click({ clickCount: 3 }); // Triple click to select contents of input
    await valueInput.type('-150');
    const value = await page.evaluate((el) => el.value, valueInput);
    await valueInput.dispose();

    expect(value).toBe('0150');
  });

  it('I want to enter a sale of 150 EUR into the system', async () => {
    await page.goto(process.env.TEST_ENDPOINT);

    // Click sell button
    await page.click('#EUR td:nth-of-type(4) button');

    // Clear field and enter new value
    const valueInput = await page.$('#amount');
    await valueInput.click({ clickCount: 3 }); // Triple click to select contents of input
    await valueInput.type('150');
    await valueInput.dispose();

    // Click 'Buy' button
    await page.click('#transactionButton');

    // Read current stock after purchase
    const stockCell = await page.$('#EUR td:nth-of-type(2)');
    const stock = await page.evaluate(
      (el) => parseInt(el.textContent),
      stockCell
    );

    // Assert value
    expect(stock).toBe(1150);
  });

  it('I want to switch from buy to sell in the popup form', async () => {
    await page.goto(process.env.TEST_ENDPOINT);

    // Click buy button
    await page.click('#EUR td:nth-of-type(3) button');

    // Click "Sell EUR" button
    await page.click('#sell');

    // Check transaction button text
    const txButton = await page.$('#transactionButton');
    expect(await page.evaluate((el) => el.textContent, txButton)).toBe(
      'Sell EUR'
    );
  });

  it('I want to be prevented from selling more of a currency than I have', async () => {
    await page.goto(process.env.TEST_ENDPOINT);

    // Click buy button
    await page.click('#EUR td:nth-of-type(3) button');

    // Clear field and enter new value
    const valueInput = await page.$('#amount');
    await valueInput.click({ clickCount: 3 }); // Triple click to select contents of input
    await valueInput.type('1500');
    await valueInput.dispose();

    // Click transaction button
    await page.click('#transactionButton');

    // Read current stock after purchase
    const stockCell = await page.$('#EUR td:nth-of-type(2)');
    const stock = await page.evaluate(
      (el) => parseInt(el.textContent),
      stockCell
    );

    // Assert the stock hasn't changed
    expect(stock).toBe(1000);
  });

  afterEach(async () => {
    await browser.close();
  });
});
