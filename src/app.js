// React
import React from 'react';
import ReactDOM from 'react-dom';

// Styles
import { ThemeProvider } from 'react-jss';
import theme from './theme';

// Redux
import { Provider } from 'react-redux';
import createStore from './redux';
const store = createStore();

store.subscribe(() => {
  const state = store.getState();
  localStorage.setItem('currency', JSON.stringify(state.currency));
  localStorage.setItem('configuration', JSON.stringify(state.configuration));
});

// Components
import App from './components/App';

// Prepare render
const jsx = (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  </Provider>
);
const container = document.getElementById('app');

// Render
if (container !== null) {
  ReactDOM.render(jsx, container);
}
