export const getCurrency = (state, cid) =>
  state.currency.filter(({ id }) => id === cid)[0];

export const getCurrencyRates = (state) =>
  state.currency.map(({ id, rate }) => ({ id, rate }));

export const getBaseCurrency = (state) => state.configuration.baseCurrency;

export const getCurrencyList = (state) => state.configuration.currencyList;

export const getMargin = (state) => state.configuration.margin;

export const getRefreshRate = (state) => state.configuration.refreshRate;

export const getCommissionParams = (state) => ({
  commissionPct: state.configuration.commissionPct,
  surcharge: state.configuration.surcharge,
  minCommission: state.configuration.minCommission
});
