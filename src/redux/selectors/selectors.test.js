import {
  getCurrency,
  getBaseCurrency,
  getCurrencyList,
  getMargin,
  getRefreshRate,
  getCommissionParams
} from './';

describe('the redux selectors', () => {
  const currency = [
    { id: 'EUR', amount: 500 },
    { id: 'CAD', amount: 200 },
    { id: 'GBP', amount: 100 }
  ];
  const configuration = {
    baseCurrency: 'USD',
    currencyList: ['CAD', 'EUR', 'GBP'],
    margin: 0.05,
    refreshRate: 30000,
    commissionPct: 0.1,
    surcharge: 0.3,
    minCommission: 1
  };
  const store = {
    currency,
    configuration
  };

  describe('the currency selector', () => {
    it('should select the correct currency', () => {
      expect(getCurrency(store, 'EUR')).toEqual({
        id: 'EUR',
        amount: 500
      });
    });
  });

  describe('the base currency selector', () => {
    it('should return the base currency', () => {
      expect(getBaseCurrency(store)).toBe('USD');
    });
  });

  describe('the currency list selector', () => {
    it('should return the list of supported currencies', () => {
      expect(getCurrencyList(store)).toEqual(['CAD', 'EUR', 'GBP']);
    });
  });

  describe('the margin selector', () => {
    it('should return the current margin', () => {
      expect(getMargin(store)).toEqual(0.05);
    });
  });

  describe('the refresh rate selector', () => {
    it('should return the current refresh rate', () => {
      expect(getRefreshRate(store)).toEqual(30000);
    });
  });

  describe('the commission info selector', () => {
    it('should return the commisionPct, surcharge, and min commission', () => {
      expect(getCommissionParams(store)).toEqual({
        commissionPct: 0.1,
        surcharge: 0.3,
        minCommission: 1
      });
    });
  });
});
