import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';

// Reducers
import reducer from './reducers';

// Sagas
import sagas from './sagas';

export default () => {
  // Setup middleware/enhancers
  const composeEnhancers =
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

  const sagaMiddleware = createSagaMiddleware();
  const middleware = applyMiddleware(sagaMiddleware);
  const enhancer = composeEnhancers(middleware);

  // Create Store
  const store = createStore(reducer, enhancer);

  // Run sagas
  sagaMiddleware.run(sagas);

  return store;
};
