// Action to be captured by a validation saga
export const setMargin = (value) => ({
  type: 'VALIDATE_MARGIN',
  value
});

//
export const setMarginSaga = (value) => ({
  type: 'SET_MARGIN',
  value
});

export const setCommissionPct = (value) => ({
  type: 'VALIDATE_COMMISSION_PCT',
  value
});

export const setCommissionPctSaga = (value) => ({
  type: 'SET_COMMISSION_PCT',
  value
});

export const setSurcharge = (value) => ({
  type: 'SET_SURCHARGE',
  value
});

export const setMinCommission = (value) => ({
  type: 'SET_MIN_COMMISSION',
  value
});

export const setRefreshRate = (value) => ({
  type: 'SET_REFRESH_RATE',
  value
});

export const setLastUpdateSaga = (value) => ({
  type: 'SET_LAST_UPDATE',
  value
});

export const setModalOpen = (action, currency) => ({
  type: 'SET_MODAL_OPEN',
  modal: {
    action,
    currency,
    isOpen: currency !== ''
  }
});
