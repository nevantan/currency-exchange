import {
  addCurrency,
  buyCurrencyAction,
  sellCurrencyAction,
  setRates,
  updateCurrency
} from './currency';

describe('the BUY_CURRENCY action object creator', () => {
  it('should return a well-formed action object', () => {
    const action = {
      type: 'BUY_CURRENCY',
      id: 'EUR',
      amount: 200
    };

    expect(buyCurrencyAction(action.id, action.amount)).toEqual(action);
  });
});

describe('the SELL_CURRENCY action object creator', () => {
  it('should return a well-formed action object', () => {
    const action = {
      type: 'SELL_CURRENCY',
      id: 'EUR',
      amount: 200
    };

    expect(sellCurrencyAction(action.id, action.amount)).toEqual(action);
  });
});

describe('the ADD_CURRENCY action object creator', () => {
  it('should return a well-formed action object', () => {
    const id = 'EUR';
    const amount = 200;

    expect(addCurrency(id, amount)).toEqual({
      type: 'ADD_CURRENCY',
      id,
      amount
    });
  });
});

describe('the SET_RATES action object creator', () => {
  it('should return a well-formed action object', () => {
    const rates = [{ id: 'EUR', rate: 1.1344 }];

    expect(setRates(rates)).toEqual({
      type: 'SET_RATES',
      rates: rates
    });
  });
});

describe('the UPDATE_CURRENCY action object creator', () => {
  it('should return a well-formed action object', () => {
    const id = 'EUR';
    const updates = {
      amount: 700,
      rate: 1.14
    };

    expect(updateCurrency(id, updates)).toEqual({
      type: 'UPDATE_CURRENCY',
      id,
      updates
    });
  });
});
