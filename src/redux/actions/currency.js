export const addCurrency = (id, amount) => ({
  type: 'ADD_CURRENCY',
  id,
  amount
});

export const buyCurrencyAction = (id, amount) => ({
  type: 'BUY_CURRENCY',
  id,
  amount
});

export const sellCurrencyAction = (id, amount) => ({
  type: 'SELL_CURRENCY',
  id,
  amount
});

export const setRates = (rates) => ({
  type: 'SET_RATES',
  rates
});

export const updateCurrency = (id, updates) => ({
  type: 'UPDATE_CURRENCY',
  id,
  updates
});
