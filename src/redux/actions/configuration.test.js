import {
  setMargin,
  setMarginSaga,
  setCommissionPct,
  setCommissionPctSaga,
  setSurcharge,
  setMinCommission,
  setRefreshRate,
  setLastUpdateSaga,
  setModalOpen
} from './configuration';

describe('the SET_MARGIN action object creator', () => {
  it('should return a well-formed validation object', () => {
    const value = 0.05;

    expect(setMargin(value)).toEqual({
      type: 'VALIDATE_MARGIN',
      value
    });
  });

  it('should return a well-formed setter object', () => {
    const value = 0.05;

    expect(setMarginSaga(value)).toEqual({
      type: 'SET_MARGIN',
      value
    });
  });
});

describe('the SET_COMMISSION_PCT action object creator', () => {
  it('should return a well-formed validation object', () => {
    const value = 0.1;

    expect(setCommissionPct(value)).toEqual({
      type: 'VALIDATE_COMMISSION_PCT',
      value
    });
  });

  it('should return a well-formed setter object', () => {
    const value = 0.1;

    expect(setCommissionPctSaga(value)).toEqual({
      type: 'SET_COMMISSION_PCT',
      value
    });
  });
});

describe('the SET_SURCHARGE action object creator', () => {
  it('should return a well-formed action object', () => {
    const value = 0.3;

    expect(setSurcharge(value)).toEqual({
      type: 'SET_SURCHARGE',
      value
    });
  });
});

describe('the SET_MIN_COMMISSION action object creator', () => {
  it('should return a well-formed action object', () => {
    const value = 1.1;

    expect(setMinCommission(value)).toEqual({
      type: 'SET_MIN_COMMISSION',
      value
    });
  });
});

describe('the SET_REFRESH_RATE action object creator', () => {
  it('should return a well-formed action object', () => {
    const value = 1000;

    expect(setRefreshRate(value)).toEqual({
      type: 'SET_REFRESH_RATE',
      value
    });
  });
});

describe('the SET_LAST_UPDATE action object creator', () => {
  it('should return a well-formed setter object', () => {
    const value = 1550885919151;

    expect(setLastUpdateSaga(value)).toEqual({
      type: 'SET_LAST_UPDATE',
      value
    });
  });
});

describe('the SET_MODAL_OPEN action object creator', () => {
  it('should return a well-formed setter object', () => {
    const action = 'buy';
    const currency = 'EUR';

    expect(setModalOpen(action, currency)).toEqual({
      type: 'SET_MODAL_OPEN',
      modal: {
        action,
        currency,
        isOpen: true
      }
    });
  });
});
