import reducer, { defaultState } from './configuration';

describe('the configuration reducer', () => {
  const config = {
    margin: 0.05,
    commissionPct: 0.1,
    surcharge: 0.3,
    minCommission: 1,
    refreshRate: 10000
  };

  it('should initialize with default state', () => {
    expect(reducer(undefined, { type: '@@INIT' })).toEqual(defaultState);
  });

  it('should set the margin', () => {
    const value = 0.02;
    const action = { type: 'SET_MARGIN', value };

    const state = reducer(config, action);
    expect(state.margin).toBe(value);
  });

  it('should set the commission percentage', () => {
    const value = 0.2;
    const action = { type: 'SET_COMMISSION_PCT', value };

    const state = reducer(config, action);
    expect(state.commissionPct).toBe(value);
  });

  it('should set the surcharge', () => {
    const value = 0.25;
    const action = { type: 'SET_SURCHARGE', value };

    const state = reducer(config, action);
    expect(state.surcharge).toBe(value);
  });

  it('should set the minimum commission', () => {
    const value = 0.5;
    const action = { type: 'SET_MIN_COMMISSION', value };

    const state = reducer(config, action);
    expect(state.minCommission).toBe(value);
  });

  it('should set the refreshRate', () => {
    const value = 60000;
    const action = { type: 'SET_REFRESH_RATE', value };

    const state = reducer(config, action);
    expect(state.refreshRate).toBe(value);
  });

  it('should set the last updated date', () => {
    const value = 1550885919151;
    const action = { type: 'SET_LAST_UPDATE', value };

    const state = reducer(config, action);
    expect(state.lastUpdate).toBe(value);
  });
});
