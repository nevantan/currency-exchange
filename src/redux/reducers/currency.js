import { currencies } from '../../../config.json';

export let defaultState = [...currencies];

const currency = localStorage.getItem('currency');
if (currency !== null) {
  defaultState = JSON.parse(currency);
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'ADD_CURRENCY':
      return state.map((currency) => {
        if (currency.id === action.id) {
          return {
            ...currency,
            amount: currency.amount + action.amount
          };
        } else {
          return currency;
        }
      });
    case 'UPDATE_CURRENCY':
      return state.map((currency) => {
        if (currency.id === action.id) {
          return {
            ...currency,
            ...action.updates
          };
        } else {
          return currency;
        }
      });
    default:
      return state;
  }
};
