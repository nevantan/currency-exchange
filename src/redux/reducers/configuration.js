import { configuration } from '../../../config.json';

export let defaultState = {
  ...configuration,
  lastUpdate: 0,
  modal: {
    isOpen: false,
    currency: '',
    action: 'buy'
  }
};

const config = localStorage.getItem('configuration');
if (config !== null) {
  defaultState = JSON.parse(config);
}

export default (state = defaultState, action) => {
  switch (action.type) {
    case 'SET_MARGIN':
      return {
        ...state,
        margin: action.value
      };
    case 'SET_COMMISSION_PCT':
      return {
        ...state,
        commissionPct: action.value
      };
    case 'SET_SURCHARGE':
      return {
        ...state,
        surcharge: action.value
      };
    case 'SET_MIN_COMMISSION':
      return {
        ...state,
        minCommission: action.value
      };
    case 'SET_REFRESH_RATE':
      return {
        ...state,
        refreshRate: action.value
      };
    case 'SET_LAST_UPDATE':
      return {
        ...state,
        lastUpdate: action.value
      };
    case 'SET_MODAL_OPEN':
      return {
        ...state,
        modal: action.modal
      };
    default:
      return state;
  }
};
