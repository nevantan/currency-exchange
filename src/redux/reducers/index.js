import { combineReducers } from 'redux';
import configuration from './configuration';
import currency from './currency';

export default combineReducers({
  configuration,
  currency
});
