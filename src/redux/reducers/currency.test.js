import reducer, { defaultState } from './currency';

describe('the currency reducer', () => {
  const currencies = [
    { id: 'EUR', amount: 500, rate: 1.13 },
    { id: 'USD', amount: 700, rate: 1 }
  ];

  it('should initialize with default state', () => {
    const action = { type: '@@INIT' };

    const state = reducer(undefined, action);
    expect(state).toEqual(defaultState);
  });

  it('should add currency', () => {
    const id = 'EUR';
    const amount = 200;
    const action = { type: 'ADD_CURRENCY', id, amount };

    const state = reducer(currencies, action);
    expect(state[0].amount).toBe(700);
  });

  it('should subtract currency', () => {
    const id = 'USD';
    const amount = -100;
    const action = { type: 'ADD_CURRENCY', id, amount };

    const state = reducer(currencies, action);
    expect(state[1].amount).toBe(600);
  });

  it('should apply updates to a currency', () => {
    const id = 'EUR';
    const updates = {
      amount: 700,
      rate: 1.14
    };
    const action = { type: 'UPDATE_CURRENCY', id, updates };

    const state = reducer(currencies, action);
    expect(state[0].amount).toBe(700);
    expect(state[0].rate).toBe(1.14);
  });
});
