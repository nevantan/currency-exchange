import { call, put, takeEvery } from 'redux-saga/effects';
import { setMarginSaga, setCommissionPctSaga } from '../actions/configuration';

export function* formatMargin(margin) {
  if (typeof margin !== 'number') margin = 0;

  return Math.abs(margin);
}

export function* setMargin({ value }) {
  const margin = yield call(formatMargin, value);
  yield put(setMarginSaga(margin));
}

export function* watchValidateMargin() {
  yield takeEvery('VALIDATE_MARGIN', setMargin);
}
