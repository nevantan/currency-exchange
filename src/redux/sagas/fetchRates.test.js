import { call, delay, put, select } from 'redux-saga/effects';
import {
  format,
  fetchRates,
  periodicallyFetchRates,
  generateStochastic
} from './fetchRates';
import { updateCurrency } from '../actions/currency';
import { setLastUpdateSaga } from '../actions/configuration';
import {
  getBaseCurrency,
  getCurrencyList,
  getMargin,
  getRefreshRate,
  getCurrencyRates
} from '../selectors';

// Sample currency/amount pairs
const currencies = [
  { id: 'CAD', amount: 200 },
  { id: 'EUR', amount: 500 },
  { id: 'GBP', amount: 100 }
];

// Sample currency/rate pairs
const rates = [
  { id: 'CAD', rate: 0.7586, buy: 0.7965, sell: 0.7207 },
  { id: 'EUR', rate: 1.1342, buy: 1.1909, sell: 1.0775 },
  { id: 'GBP', rate: 1.3031, buy: 1.3683, sell: 1.2379 }
];

// Sample API response
const res = {
  base: 'USD',
  date: '2019-02-20',
  rates: {
    CAD: 1.31821777,
    EUR: 0.8816787163,
    GBP: 0.767400813
  }
};

const margin = 0.05;

const lastRates = [
  { id: 'CAD', rate: 0.7586 },
  { id: 'EUR', rate: 1.1342 },
  { id: 'GBP', rate: 1.3031 }
];

describe('the fetchRates utils', () => {
  it('should format the currency list', () => {
    expect(format(res.rates, margin, lastRates, 0)).toEqual(rates);
  });
});

describe('the fetchRates saga', () => {
  it('should fetch currency rates from remote api', () => {
    const fetchCur = ['CAD', 'EUR', 'GBP'];
    const saga = fetchRates('USD', fetchCur);
    const endpoint = `${process.env.API_ENDPOINT}?base=USD&symbols=CAD,EUR,GBP`;

    // Make fetch request
    expect(saga.next().value).toEqual(call(fetch, endpoint));

    // Convert fetch result to JSON
    const resObj = {
      json: () => res
    };
    expect(saga.next(resObj).value).toEqual(res);

    // Select margin value
    expect(saga.next(res).value).toEqual(select(getMargin));

    // Select last rates
    expect(saga.next(margin).value).toEqual(select(getCurrencyRates));

    // Generate random value
    expect(saga.next(lastRates).value).toEqual(call(generateStochastic));

    // Dispatch rate updates
    expect(saga.next(0.01).value).toEqual(
      put(updateCurrency('CAD', { rate: 0.7662, buy: 0.8045, sell: 0.7279 }))
    );
    expect(saga.next().value).toEqual(
      put(updateCurrency('EUR', { rate: 1.1455, buy: 1.2028, sell: 1.0882 }))
    );
    expect(saga.next().value).toEqual(
      put(updateCurrency('GBP', { rate: 1.3161, buy: 1.3819, sell: 1.2503 }))
    );

    // Get current time
    expect(saga.next().value).toEqual(call(Date.now));

    // Dispatch last updated action
    const time = Date.now();
    expect(saga.next(time).value).toEqual(put(setLastUpdateSaga(+time)));
  });
});

describe('the periodicallyFetchRates saga', () => {
  it('should periodically fetch currency rates', () => {
    const t = 1000;
    const saga = periodicallyFetchRates();

    // Get Base Currency
    expect(saga.next().value).toEqual(select(getBaseCurrency));

    // Get Supported Currencies
    expect(saga.next('USD').value).toEqual(select(getCurrencyList));

    // Get Refresh Rate
    expect(saga.next(['EUR', 'CAD', 'GBP']).value).toEqual(
      select(getRefreshRate)
    );

    // Fetch Rates
    expect(saga.next(t).value).toEqual(
      call(fetchRates, 'USD', ['EUR', 'CAD', 'GBP'], t)
    );

    // Delay
    expect(saga.next().value).toEqual(delay(t));

    // Get Refresh Rate
    expect(saga.next().value).toEqual(select(getRefreshRate));

    // Fetch Rates
    expect(saga.next(t).value).toEqual(
      call(fetchRates, 'USD', ['EUR', 'CAD', 'GBP'], t)
    );

    // Delay
    expect(saga.next().value).toEqual(delay(t));
  });
});
