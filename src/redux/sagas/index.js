// Redux Saga
import { all, fork } from 'redux-saga/effects';

// Intent Implementors
import { watchBuyCurrency } from './buyCurrency';
import { watchSellCurrency } from './sellCurrency';

// Validators
import { watchValidateMargin } from './validateMargin';
import { watchValidateCommissionPct } from './validateCommissionPct';

// Background Actions
import { periodicallyFetchRates } from './fetchRates';

export default function* rootSaga() {
  yield fork(periodicallyFetchRates);
  yield all([
    watchBuyCurrency(),
    watchSellCurrency(),
    watchValidateMargin(),
    watchValidateCommissionPct()
  ]);
}
