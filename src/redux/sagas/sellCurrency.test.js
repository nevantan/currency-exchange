import { call, put, select, takeEvery } from 'redux-saga/effects';
import { validate, sellCurrency, watchSellCurrency } from './sellCurrency';
import { sellCurrencyAction, updateCurrency } from '../actions/currency';
import { getCurrency, getCommissionParams } from '../selectors';

describe('the validation utility', () => {
  const currencies = [
    { id: 'EUR', amount: 500, rate: 1.13 },
    { id: 'CAD', amount: 200, rate: 0.75 },
    { id: 'GBP', amount: 100, rate: 1.3 },
    { id: 'USD', amount: 1000, rate: 1.0 }
  ];

  it('should return true for sufficient funds', () => {
    const saga = validate('EUR', 200);
    expect(saga.next().value).toEqual(select(getCurrency, 'EUR'));
    expect(saga.next(currencies[0]).value).toEqual(select(getCurrency, 'USD'));
    expect(saga.next(currencies[3]).value).toBe(true); // Provide mocked EUR value to next()
  });

  it('should return false for insufficient stock', () => {
    const saga = validate('EUR', 1000);
    expect(saga.next().value).toEqual(select(getCurrency, 'EUR'));
    expect(saga.next().value).toEqual(select(getCurrency, 'USD'));
    expect(saga.next(currencies[0]).value).toBe(false); // Provide mocked CAD value to next()
  });

  it('should return false for non-supported currency', () => {
    const saga = validate('FOO', 100);
    expect(saga.next().value).toEqual(select(getCurrency, 'FOO'));
    expect(saga.next().value).toEqual(select(getCurrency, 'USD'));
    expect(saga.next(undefined).value).toBe(false); // Provide mocked FOO value to next()
  });

  it('should reject zero-amount transactions', () => {
    const saga = validate('EUR', 0);
    expect(saga.next().value).toEqual(false);
    expect(saga.next().done).toBe(true);
  });

  it('should reject negative sell transactions', () => {
    const saga = validate('EUR', -500);
    expect(saga.next().value).toEqual(false);
    expect(saga.next().done).toBe(true);
  });
});

describe('the sellCurrency saga', () => {
  it('should validate the transaction, then dispatch stock adjustments', () => {
    const saga = sellCurrency(sellCurrencyAction('EUR', 200));

    // Validates the transaction
    expect(saga.next().value).toEqual(call(validate, 'EUR', 200));

    // Get exchange rate of the foreign currency
    expect(saga.next(true).value).toEqual(select(getCurrency, 'EUR'));

    // Increase the stock of the foreign currency (mock validation result to true)
    expect(saga.next({ amount: 500, rate: 1.13 }).value).toEqual(
      put(updateCurrency('EUR', { amount: 700 }))
    );

    // Get the current amount of USD
    expect(saga.next().value).toEqual(select(getCurrency, 'USD'));

    // Get commission params
    expect(saga.next({ amount: 1000 }).value).toEqual(
      select(getCommissionParams)
    );

    // Remove USD from purchase (mock USD/EUR rate to 1.13)
    expect(
      saga.next({ commissionPct: 0.1, surcharge: 0.3, minCommission: 1 }).value
    ).toEqual(put(updateCurrency('USD', { amount: 796.9 })));
  });

  it('rejects an invalid transaction without dispatching stock adjustments', () => {
    const saga = sellCurrency(sellCurrencyAction('FOO', -500));

    // Validates the transaction
    expect(saga.next().value).toEqual(call(validate, 'FOO', -500));

    // Rejects an invalid transaction (mock validation result to false)
    expect(saga.next(false).done).toBe(true);
  });
});

describe('the watchSellCurrency saga', () => {
  it('should take all SELL_CURRENCY actions and implement them', () => {
    const saga = watchSellCurrency();

    expect(saga.next().value).toEqual(takeEvery('SELL_CURRENCY', sellCurrency));
  });
});
