import { call, put, takeEvery } from 'redux-saga/effects';
import { setMarginSaga, setCommissionPctSaga } from '../actions/configuration';

export function* formatCommissionPct(value) {
  if (isNaN(+value)) value = 0;

  return value;
}

export function* setCommissionPct({ value }) {
  const commissionPct = yield call(formatCommissionPct, value);
  yield put(setCommissionPctSaga(commissionPct));
}

export function* watchValidateCommissionPct() {
  yield takeEvery('VALIDATE_COMMISSION_PCT', setCommissionPct);
}
