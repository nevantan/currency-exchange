import {
  formatCommissionPct,
  setCommissionPct,
  watchValidateCommissionPct
} from './validateCommissionPct';
import { setMarginSaga, setCommissionPctSaga } from '../actions/configuration';
import { call, put, takeEvery } from 'redux-saga/effects';

describe('the configuration validation sagas', () => {
  describe('the commission percentage validation saga', () => {
    it('defaults a non-number to 0', () => {
      const saga = formatCommissionPct('foo');
      expect(saga.next().value).toBe(0);
    });

    it('sets a valid commission pct', () => {
      const value = 0.1;
      const saga = setCommissionPct({ value });
      expect(saga.next().value).toEqual(call(formatCommissionPct, value));
      expect(saga.next(0.1).value).toEqual(put(setCommissionPctSaga(value)));
    });

    it('sets a formatted string commission pct', () => {
      const value = 'foo';
      const saga = setCommissionPct({ value });
      expect(saga.next().value).toEqual(call(formatCommissionPct, value));
      expect(saga.next(0).value).toEqual(put(setCommissionPctSaga(0)));
    });
  });
});
