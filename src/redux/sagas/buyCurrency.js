import { call, put, select, takeEvery } from 'redux-saga/effects';
import { updateCurrency } from '../actions/currency';
import { getCurrency, getCommissionParams } from '../selectors';

export function* validate(id, amount) {
  // Reject zero- and negative-amount transactions
  if (amount <= 0) {
    return false;
  }

  // Fetch current stock of currency from store
  const currency = yield select(getCurrency, id);

  // If the currency exists and there is a sufficient amount
  if (currency && currency.amount >= amount) {
    return true;
  } else {
    return false;
  }
}

export function* buyCurrency({ id, amount }) {
  // Validate transaction
  const valid = yield call(validate, id, amount);

  // Reject an invalid transaction
  if (!valid) return;

  // Fetch the amount and rate of current currency
  const { amount: curAmount, rate } = yield select(getCurrency, id);

  // Dispatch the removal of the foreign currency
  const updates = {
    amount: curAmount - amount
  };
  yield put(updateCurrency(id, updates));

  // Get amount USD
  const { amount: curUSD } = yield select(getCurrency, 'USD');

  // Get commission info
  const { commissionPct, surcharge, minCommission } = yield select(
    getCommissionParams
  );

  // Calculate USD value with commission
  const usd = amount * rate;
  const commission = Math.max(usd * commissionPct + surcharge, minCommission);
  const rounded = Math.round((usd + commission) * 100) / 100;

  // Dispatch the addition of USD from the sale
  yield put(updateCurrency('USD', { amount: curUSD + rounded }));
}

export function* watchBuyCurrency() {
  yield takeEvery('BUY_CURRENCY', buyCurrency);
}
