import { call, delay, put, select } from 'redux-saga/effects';
import { updateCurrency } from '../actions/currency';
import { setLastUpdateSaga } from '../actions/configuration';
import {
  getRefreshRate,
  getBaseCurrency,
  getCurrencyList,
  getMargin,
  getCurrencyRates
} from '../selectors';

// Formats the API response into an array of currency/rate pairs
export const format = (rates, margin, lastRates, stochastic) => {
  // For each 'quote' value returned
  return Object.entries(rates).map(([id, rateUSD], i) => {
    // Calculate the USD rate to 4 decimals
    let rate = Math.round((1 / rateUSD) * 10000) / 10000;

    // Find last rate
    const last = lastRates.filter((rate) => rate.id === id)[0].rate;

    // Apply stochastic and round again
    if (rate === last) {
      rate *= 1 + stochastic;
      rate = Math.round(rate * 10000) / 10000;
    }

    // Calculate buy/sell rates with margin to 4 decimals
    const buy = Math.round(rate * (1 + margin) * 10000) / 10000;
    const sell = Math.round(rate * (1 - margin) * 10000) / 10000;

    // Return currency/rate pair
    return {
      id,
      rate,
      buy,
      sell
    };
  });
};

export function* generateStochastic() {
  return Math.random() * 0.03 - 0.015;
}

// Single fetch action to API
export function* fetchRates(base, currencies, refresh) {
  try {
    // Make fetch request
    const response = yield call(
      fetch,
      `${process.env.API_ENDPOINT}?base=${base}&symbols=${currencies.join(',')}`
    );

    // Convert fetch to JSON
    const data = yield response.json();

    // Select margin
    const margin = yield select(getMargin);

    // Select currency rates
    const lastRates = yield select(getCurrencyRates);

    // Generate random value (+/-1.5%)
    const stochastic = yield call(generateStochastic);

    // Format API response to currency/rate pairs
    const formatted = format(data.rates, margin, lastRates, stochastic);

    // Dispatch each rate to store
    for (let i = 0; i < formatted.length; i++) {
      const { id, ...updates } = formatted[i];
      yield put(updateCurrency(id, updates));
    }

    // Get current time
    const time = yield call(Date.now);

    // Set last update
    yield put(setLastUpdateSaga(+time));

    // Return same refresh rate
    return refresh;
  } catch (e) {
    console.log(
      `Failed to fetch currencies, retrying in ${refresh} seconds...`
    );
  }
}

// Saga to periodically trigger the fetchRates saga
export function* periodicallyFetchRates() {
  // Get base currency code
  const base = yield select(getBaseCurrency);

  // Get list of supported currencies
  const currencies = yield select(getCurrencyList);

  while (true) {
    // Get refresh rate value
    const refresh = yield select(getRefreshRate);

    // Fetch rates from API if not disabled
    if (refresh > 0) {
      yield call(fetchRates, base, currencies, refresh);
    }

    // Delay next call for `refresh` ms
    yield delay(refresh);
  }
}
