import { formatMargin, setMargin, watchValidateMargin } from './validateMargin';
import { setMarginSaga, setCommissionPctSaga } from '../actions/configuration';
import { call, put, takeEvery } from 'redux-saga/effects';

describe('the margin validation sagas', () => {
  it('auto-formats negative margins', () => {
    const saga = formatMargin(-0.05);
    expect(saga.next().value).toBe(0.05);
  });

  it('defaults a non-number margin to 0', () => {
    const saga = formatMargin('foo');
    expect(saga.next().value).toBe(0);
  });

  it('sets a valid margin', () => {
    const value = 0.05;
    const saga = setMargin({ value });
    expect(saga.next().value).toEqual(call(formatMargin, value));
    expect(saga.next(0.05).value).toEqual(put(setMarginSaga(value)));
  });

  it('sets a formatted negative margin', () => {
    const value = -0.05;
    const saga = setMargin({ value });
    expect(saga.next().value).toEqual(call(formatMargin, value));
    expect(saga.next(0.05).value).toEqual(put(setMarginSaga(0.05)));
  });

  it('sets a formatted string margin', () => {
    const value = 'foo';
    const saga = setMargin({ value });
    expect(saga.next().value).toEqual(call(formatMargin, value));
    expect(saga.next(0).value).toEqual(put(setMarginSaga(0)));
  });

  it('captures all SET_MARGIN actions', () => {
    const saga = watchValidateMargin();
    expect(saga.next().value).toEqual(takeEvery('VALIDATE_MARGIN', setMargin));
  });
});
