import { call, put, select, takeEvery } from 'redux-saga/effects';
import { validate, buyCurrency, watchBuyCurrency } from './buyCurrency';
import { buyCurrencyAction, updateCurrency } from '../actions/currency';
import { getCurrency, getCommissionParams } from '../selectors';

describe('the validation utility', () => {
  const currencies = [
    { id: 'EUR', amount: 500 },
    { id: 'CAD', amount: 200 },
    { id: 'GBP', amount: 100 }
  ];

  it('should return true for sufficient stock', () => {
    const saga = validate('EUR', 200);
    expect(saga.next().value).toEqual(select(getCurrency, 'EUR'));
    expect(saga.next(currencies[0]).value).toBe(true); // Provide mocked EUR value to next()
  });

  it('should return false for insufficient stock', () => {
    const saga = validate('CAD', 300);
    expect(saga.next().value).toEqual(select(getCurrency, 'CAD'));
    expect(saga.next(currencies[1]).value).toBe(false); // Provide mocked CAD value to next()
  });

  it('should return false for non-supported currency', () => {
    const saga = validate('FOO', 100);
    expect(saga.next().value).toEqual(select(getCurrency, 'FOO'));
    expect(saga.next(undefined).value).toBe(false); // Provide mocked FOO value to next()
  });

  it('should reject zero-amount transactions', () => {
    const saga = validate('EUR', 0);
    expect(saga.next().value).toEqual(false);
    expect(saga.next().done).toBe(true);
  });

  it('should reject negative buy transactions', () => {
    const saga = validate('EUR', -500);
    expect(saga.next().value).toEqual(false);
    expect(saga.next().done).toBe(true);
  });
});

describe('the buyCurrency saga', () => {
  it('should validate the transaction, then dispatch stock adjustments', () => {
    const saga = buyCurrency(buyCurrencyAction('EUR', 200));

    // Validates the transaction
    expect(saga.next().value).toEqual(call(validate, 'EUR', 200));

    // Get exchange rate of the current currency
    expect(saga.next(true).value).toEqual(select(getCurrency, 'EUR'));

    // Reduce stock of the purchased currency (mock validation result to true)
    expect(saga.next({ amount: 500, rate: 1.13 }).value).toEqual(
      put(updateCurrency('EUR', { amount: 300 }))
    );

    // Get USD amount
    expect(saga.next().value).toEqual(select(getCurrency, 'USD'));

    // Get commission params
    expect(saga.next({ amount: 1000 }).value).toEqual(
      select(getCommissionParams)
    );

    // Add USD from sale (mock USD/EUR rate to 1.13)
    expect(
      saga.next({ commissionPct: 0.1, surcharge: 0.3, minCommission: 1 }).value
    ).toEqual(put(updateCurrency('USD', { amount: 1248.9 })));
  });

  it('rejects an invalid transaction without dispatching stock adjustments', () => {
    const saga = buyCurrency(buyCurrencyAction('FOO', -500));

    // Validates the transaction
    expect(saga.next().value).toEqual(call(validate, 'FOO', -500));

    // Rejects an invalid transaction (mock validation result to false)
    expect(saga.next(false).done).toBe(true);
  });
});

describe('the watchBuyCurrency saga', () => {
  it('should take all BUY_CURRENCY actions and implement them', () => {
    const saga = watchBuyCurrency();

    expect(saga.next().value).toEqual(takeEvery('BUY_CURRENCY', buyCurrency));
  });
});
