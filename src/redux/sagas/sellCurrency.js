import { call, put, select, takeEvery } from 'redux-saga/effects';
import { updateCurrency } from '../actions/currency';
import { getCurrency, getCommissionParams } from '../selectors';

export function* validate(id, amount) {
  // Reject zero- and negative-amount transactions
  if (amount <= 0) {
    return false;
  }

  // Fetch foreign currency rate from store
  const currency = yield select(getCurrency, id);

  // Fetch USD stock from store
  const usd = yield select(getCurrency, 'USD');

  // If the currency exists and there is a sufficient amount
  if (currency && amount * currency.rate <= usd.amount) {
    return true;
  } else {
    return false;
  }
}

export function* sellCurrency({ id, amount }) {
  // Validate transaction
  const valid = yield call(validate, id, amount);

  // Reject an invalid transaction
  if (!valid) return;

  // Fetch the current currency info
  const { amount: curAmount, rate } = yield select(getCurrency, id);

  // Dispatch the addition of the foreign currency
  yield put(updateCurrency(id, { amount: curAmount + amount }));

  // Get current USD rate
  const { amount: curUSD } = yield select(getCurrency, 'USD');

  // Get commission info
  const { commissionPct, surcharge, minCommission } = yield select(
    getCommissionParams
  );

  // Calculate USD value with commission
  const usd = amount * rate;
  const commission = Math.max(usd * commissionPct + surcharge, minCommission);
  const rounded = Math.round((usd - commission) * 100) / 100;

  // Dispatch the addition of USD from the sale
  yield put(updateCurrency('USD', { amount: curUSD - rounded }));
}

export function* watchSellCurrency() {
  yield takeEvery('SELL_CURRENCY', sellCurrency);
}
