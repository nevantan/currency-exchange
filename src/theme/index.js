import c from 'color';

const color = {
  black: '#111',
  grey: '#888',
  silver: '#ddd',
  white: '#fff',
  offwhite: '#f6f6f6',
  primary: '#ffef03',
  red: '#a00'
};

const font = {
  size: {
    xs: '1.4rem',
    s: '1.6rem',
    m: '1.8rem',
    l: '2.2rem'
  },
  family: {
    raleway: '"Raleway", sans-serif',
    sourceSans: '"Source Serif Pro", sans-serif'
  }
};

const spacing = {
  xs: '0.2rem',
  s: '0.5rem',
  m: '1.0rem',
  l: '2.0rem'
};

const button = {
  borderRadius: '1px',
  cursor: 'pointer',
  font: 'inherit',
  letterSpacing: '1px',
  padding: `${spacing.s} ${spacing.m}`
};

const common = {
  input: {
    border: `1px solid ${color.silver}`,
    borderRadius: '1px',
    padding: spacing.s
  },
  iconButton: {
    background: 'transparent',
    border: 0,
    cursor: 'pointer'
  },
  primaryButton: {
    ...button,
    backgroundColor: color.primary,
    border: `1px solid ${c(color.primary)
      .darken(0.05)
      .hex()}`,
    color: color.black
  },
  secondaryButton: {
    ...button,
    backgroundColor: 'transparent',
    border: 0
  }
};

export default {
  color,
  common,
  font,
  spacing
};
