module.exports = {
  collectCoverageFrom: [
    'src/**/*.js',
    '!src/**/style.js',
    '!src/**/story.js',
    '!src/**/app.js',
    '!src/**/theme/*',
    '!src/**/redux/index.js',
    '!src/**/reducers/index.js',
    '!src/**/*.spec.js'
  ],
  moduleNameMapper: {
    '\\.(jpg|png|svg)$': '<rootDir>/__mocks__/fileMock.js'
  },
  setupFiles: ['<rootDir>/jest.setup.js'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  testMatch: ['<rootDir>/src/**/*.test.js']
};
