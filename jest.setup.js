import dotenv from 'dotenv';
dotenv.config({
  path: '.env.test'
});

import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import fetch from 'jest-fetch-mock';
global.fetch = fetch;
